# Jenkins on k8s

## Env setup

### RPi
Jenkins is not working on arm, _yet_!

### MiniKube

To run MiniKube on your device, you need the following:

* [MiniKube](https://kubernetes.io/docs/setup/minikube/) (duh!)
* [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
* [VirtualBox](https://www.virtualbox.org/)

#### Install

##### On macOS

###### Via brew
```shell
brew install kubernetes-cli
brew cask install virtualbox minikube
```

###### Manually
Install kubectl:
```shell
curl -sSLO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl && chmod +x kubectl
```
Install MiniKube:
```shell
curl -sSLo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-amd64 && chmod +x minikube
```

#### Startup
```shell
minikube start --cpus 4 --memory 8192
```

#### Context
Check if we are running in the MiniKube Kubernetes Context.
```shell
kubectl config current-context
```
Should display `minikube`.

#### Dashboard
You can run the k8s dashboard by executing command:
```shell
minikube dashboard
```

#### Service list
To find the external URL of your exposed service, you can check the list of services
```shell
minikube service list
```
or just ask the URL for one specific service
```shell
minikube service <service name> --url
```

## Jenkins

### Deploy

#### Via `kubectl`
You can deploy all the files by executing inside the [k8s](k8s/) folder
```shell
kubectl apply -f .
```

#### Via Dashboard
Go to the MiniKube dashboard to upload the [jenkins files](k8s/) creating our Jenkins pod.

### Kubernetes plugin
Install the [Kubernetes plugin](https://plugins.jenkins.io/kubernetes) via the Jenkins Plugin Manager.

#### Configuration
![alt text](doc/img/jenkins-k8s-plugin-config-without-template.png "Kubernetes Plugin configuration")

### Fist pipeline
Create new simple pipeline with as pipeline script:
```groovy
def label = "mypod-${UUID.randomUUID().toString()}"
podTemplate(label: label) {
    node(label) {
        stage('Run shell') {
            sh 'echo "Hello DevOps!"'
        }
    }
}
```

* `label` should be unique to avoid conflicts with multiple running  builds

By default, the kubernetes plugin starts a container named `jnlp`. It will _always_ start that container.  
You can in your pod template, create a container based on image [jenkins/jnlp-slave](https://hub.docker.com/r/jenkins/jnlp-slave/) that is called `jnlp`.

You can also create your own jnlp image with the [jenkins remoting app](https://repo.jenkins-ci.org/public/org/jenkins-ci/main/remoting/) in your container.  
To run the `remoting.jar`, you should add some mandatory options that are explained in the [Jenkins Kubernetes plugin doc](https://plugins.jenkins.io/kubernetes).  
Here an example on how to execute the `remoting.jar` file.
```shell
java $JAVA_OPTS -cp /usr/share/jenkins/remoting.jar hudson.remoting.jnlp.Main -headless $TUNNEL $URL $WORKDIR $OPT_JENKINS_SECRET $OPT_JENKINS_AGENT_NAME "$@"
```
Taken from entrypoint [docker-jnlp-slave](https://github.com/jenkinsci/docker-jnlp-slave).

#### TODO: Following maybe needs better explanation
That jnlp container is used to connect to jenkins master. It is not needed to update/override/change it. Just create other container that will do the actual work. E.g.:
```groovy
def label = "mypod-${UUID.randomUUID().toString()}"
podTemplate(label: label, containers: [
    containerTemplate(name: 'maven', image: 'maven:3.6.1-jdk-8-alpine', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'golang', image: 'golang:1.8.0', ttyEnabled: true, command: 'cat')
  ]) {

    node(label) {
        stage('Get a Maven project') {
            git 'https://github.com/jenkinsci/kubernetes-plugin.git'
            container('maven') {
                stage('Build a Maven project') {
                    sh 'mvn -B clean install'
                }
            }
        }

        stage('Get a Golang project') {
            git url: 'https://github.com/hashicorp/terraform.git'
            container('golang') {
                stage('Build a Go project') {
                    sh """
                    mkdir -p /go/src/github.com/hashicorp
                    ln -s `pwd` /go/src/github.com/hashicorp/terraform
                    cd /go/src/github.com/hashicorp/terraform && make core-dev
                    """
                }
            }
        }

    }
}
```
jnlp not defined _but_ is started up and we choose to work in the other containers that we defined.

## References
* Heavily (if not fully) inspired by: http://www.darkops.io/blog/2018/01/01/local-jenkins-on-minikube.html
* Authorisation fix: https://stackoverflow.com/questions/44018842/default-service-account-not-working-with-kubernetes-plugin-on-jenkins

## TODO
* Make that part about the JNLP container much clearer!
* When using maven image, must override command (e.g.: `cat`), by default executes `mvn` but is expecting goals, so directly goes to error state.
* Add Jenkinsfile pipeline documentation
* Add Config File Management plugin section to create global `settings.xml` file
* Work with namespaces!
* https://hackernoon.com/local-kubernetes-setup-with-minikube-on-mac-os-x-eeeb1cbdc0b
* https://www.blazemeter.com/blog/how-to-setup-scalable-jenkins-on-top-of-a-kubernetes-cluster
* https://itnext.io/deploy-jenkins-with-dynamic-slaves-in-minikube-8aef5404e9c1
