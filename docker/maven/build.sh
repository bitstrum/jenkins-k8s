#!/usr/bin/env bash

VERSION="m3.6-j8-c74"
IMAGE="maacarbo/maven-chrome"

docker login
docker build --no-cache --pull --rm --tag ${IMAGE}:${VERSION} .
docker tag ${IMAGE}:${VERSION} ${IMAGE}:latest
docker push ${IMAGE}
