# Install dashboard

Check latest release here: https://github.com/kubernetes/dashboard/releases

wget https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml

Add following args in the contianer:
```
- --enable-skip-login
- --disable-settings-authorizer
```

Add to service:

Same level as `port` and `targetPort`
```
nodePort: 30001
```

Same level as `ports`:
```
type: NodePort
```
